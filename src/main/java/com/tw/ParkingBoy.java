package com.tw;

public class ParkingBoy {
    private final ParkingLot parkingLot;
    private String lastErrorMessage;

    public ParkingBoy(ParkingLot parkingLot) { this.parkingLot = parkingLot; }

    public ParkingTicket park(Car car) {
        // TODO: Implement the method according to test
        // <-start-
        ParkingResult result = parkingLot.park(car);
        if (!result.isSuccess()) {
            lastErrorMessage = parkingLot.park(car).getMessage();
        }
        return parkingLot.park(car).getTicket();
        // ---end->
    }

    public Car fetch(ParkingTicket ticket) {
        // TODO: Implement the method according to test
        // <-start-
        FetchingResult result = parkingLot.fetch(ticket);
        if (!result.isSuccess()) {
            lastErrorMessage = result.getMessage();
        }
        return result.getCar();
        // ---end->
    }

    public String getLastErrorMessage() {
        return lastErrorMessage;
    }
}
